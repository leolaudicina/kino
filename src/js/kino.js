$(document).ready(function () {
    'use strict';

    document.app = {};
    document.app.ui = {};
    document.app.data = {
        modules: {
            welcome: {
                textAbout: 'Preventa de Entradas',
                text: 'Todos los cines y tus películas favoritas ¡al mejor precio!'
            }
        }
    };
    var appData = document.app.data;

    /*
     * Se ejecuta mediante un handler en el DOM
     * y muestra un alert de Sweetalert2
     * @method      alertSuccess
     * @method      alertCheckoutSuccess
     */
    document.app.ui.alertSuccess = function (confirm) {
        if (confirm) {
            swal({
                title: '¡Listo!',
                html: 'Ya podés disfrutar de las recomendaciones que elegimos para vos.',
                type: 'success',
                confirmButtonColor: '#5622ff',
                showConfirmButton: true
            })
                .then(function () {
                    window.location.href = 'kino.home.view.html';
                })
        }
    };
    document.app.ui.alertCheckoutSuccess = function (confirm) {
        if (confirm) {
            swal({
                title: '¡Compra exitosa!',
                html: 'Te enviaremos el comprobante al correo.',
                type: 'success',
                showConfirmButton: true
            })
                .then(function () {
                    window.location.href = 'kino.checkout-end.view.html';
                })
        }
    };

    /*
 * Ajax call para COMPILAR los partials
 * utilizando un path y un callback para renderizarlo.
 *
 * @method      getTemplatesAjax
 * @param       path to the partial
 * @param       clb  callback
 */
    document.app.ui.getTemplatesAjax = function (path, clb) {
        var source, template;
        $.ajax({
            url: path,
            success: function (data) {
                source = data;
                template = Handlebars.compile(source);
                if (clb) {
                    clb(template);
                }
            }
        });
    };
    /*
     * It takes the element ID that will hold the compiled partial,
     * the partial name and an object with data to fulfill it.
     *
     * @method      renderTemplate
     * @param       elemID
     * @param       tmpl
     * @param       context
     */
    document.app.ui.renderTemplate = function (elemID, tmpl, context) {
        context = context || {};
        var route = 'partials/' + tmpl + '.hbs';

        if ($(elemID).data('template')) {
            document.app.ui.getTemplatesAjax(route, function (template) {
                $(elemID).html(template(context));
            })
        }
    };

    /*
     * @TODO        mejorar el comportamiento del datepicker
     * @method      datepicker
     */
    document.app.ui.calendar = function () {
        var $datepicker = $('.kino-ui-date-picker'),
            $arrowLeft = '<i class="fa fa-chevron-left"></i>',
            $arrowRight = '<i class="fa fa-chevron-right"></i>';

        $.fn.datepicker.defaults.language = 'es';

        $datepicker.each(function () {
            $(this).datepicker({
                minDate: new Date(),
                templates: {
                    leftArrow: $arrowLeft,
                    rightArrow: $arrowRight
                }
            }).on('show', function () {
                $('.datepicker').addClass('open animated fadeIn');
                var datepicker_color = $(this).data('datepicker-color');
                if (datepicker_color.length !== 0) {
                    $('.datepicker').addClass('datepicker-' + datepicker_color + '');
                }
            }).on('hide', function () {
                $('.datepicker').removeClass('open');
            });
        });
    };
    /*
     * Dibuja el valor correspendiente al input range
     * dentro de un <output>
     * @method      rangeWithOutput
     */
    document.app.ui.locationRange = function () {
        var el,
            newPoint,
            newPlace,
            width,
            offset,
            $rangeInput = $("input[type='range']");

        $rangeInput.change(function () {
            el = $(this);

            // Measure width of range input
            width = el.width();

            // Figure out placement percentage between left and right of input
            newPoint = (el.val() - el.attr("min")) / (el.attr("max") - el.attr("min"));
            offset = -1;

            // Prevent tooltip from going beyond left or right (unsupported browsers)
            if (newPoint < 0) {
                newPlace = 0;
            }
            else if (newPoint > 1) {
                newPlace = width;
            }
            else {
                newPlace = width * newPoint + offset;
                offset -= newPoint * 20.7;
            }

            // Move bubble
            el
                .next("output")
                .css({
                    left: newPlace,
                    marginLeft: offset + "%"
                })
                .text(el.val() + 'Km');
        })
            .trigger('change');
    };
    /*
     * Al seleccionar un género, se seleccionan todos los restantes
     * y permite continuar con el flow de preferencias.
     * @STACKOVERFLOW ISSUE     https://stackoverflow.com/questions/47745721/show-hide-content-with-multiple-checkboxes
     * @method      cardsGeneros
     */
    document.app.ui.selectGeneros = function () {
        $('.__genero').each(function () {
            $(this).on('change', function () {
                var $iconCheck = $(this).parent().find('.fa-check-circle'),
                    $imgSVG = $(this).parent().find('.__genero-img');
                document.app.ui.cardsSelected = false;

                if ($(this).is(':checked')) {
                    $iconCheck.removeClass('d-none');
                    $imgSVG.addClass('d-none');
                }
                else {
                    $iconCheck.addClass('d-none');
                    $imgSVG.removeClass('d-none');
                }

                if($('input[type=checkbox]:checked').length) {
                    document.app.ui.cardsSelected = true;
                }

                document.app.ui.isNexBtnDisabled('#btnHeaderTopNext');
            });
        });
    };

    document.app.ui.searchMovies = function () {
        var $movies = $('#moviesPreventa'),
            $noResults = $('#noResults'),
            $searchbar = $('#searchbar'),
            $visible = $movies.find('li:visible').length;

        $searchbar.on('keyup', function () {
            var li = $(this).val().toLowerCase();
            $movies.find('li').each(function () {
                var res = $(this).text().toLowerCase();
                if (res.indexOf(li) !== -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });

            if ($visible !== 0) {
                $noResults.hide();
            } else {
                $noResults.show();
            }
        });
    };

    document.app.ui.searchFilters = function () {
        var $searchInput = $('.__toggler'),
            $noResults = $('#noResults'),
            $moviesPreventa = $("#moviesPreventa"),
            $hideLink = $(".__searchform-hide"),
            $cnt = $('.__toggle-content');

        $noResults.hide();
        $hideLink.hide();

        $searchInput.on('focus', function (e) {
            e.preventDefault();
            $hideLink.show();
            $cnt.slideDown(300);
        });

        $hideLink.on('click', function (e) {
            e.preventDefault();
            $hideLink.hide();
            $cnt.slideUp(300);
        });

        if ($moviesPreventa.length > 0) {
            $searchInput.keyup(function () {
                document.app.ui.searchMovies();
            });
        }
    };

    document.app.ui.isNexBtnDisabled = function (elemID) {
        var $nextBtn = $(elemID);
        if (document.app.ui.cardsSelected) {
            $nextBtn.prop('disabled', false);
        }
        if (!document.app.ui.cardsSelected) {
            $nextBtn.prop('disabled', true);
        }
    };

    document.app.ui.showMap = function (elem, showElmID) {
        document.app.ui.mapActive = false;

        elem.forEach(function (e, i) {
            document.app.ui.mapActive = true;

            if (document.app.ui.mapActive) {
                $(showElmID).removeClass('d-none');
                $(elem[i]).addClass('d-none');
            }
            else {
                $(showElmID).addClass('d-none');
                $(elem[i]).removeClass('d-none');
            }
        });
    };

    document.app.ui.locationRange();
    document.app.ui.selectGeneros();
    document.app.ui.calendar();
    document.app.ui.searchFilters();

    document.app.ui.renderTemplate('#kinoUIHeaderNav1', 'kinoui.header-nav-1', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav2', 'kinoui.header-nav-2', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav3', 'kinoui.header-nav-3', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav4', 'kinoui.header-nav-4', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav5', 'kinoui.header-nav-5', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav6', 'kinoui.header-nav-6', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav7', 'kinoui.header-nav-7', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav8', 'kinoui.header-nav-8', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav9', 'kinoui.header-nav-9', appData);
    document.app.ui.renderTemplate('#kinoUIHeaderNav10', 'kinoui.header-nav-10', appData);

    document.app.ui.renderTemplate('#kinoUINavBottom', 'kinoui.nav-bottom', appData);
    document.app.ui.renderTemplate('#kinoUITags', 'kinoui.tags', appData);

    console.log('KINO v1.0.0 DEMO');
    console.log(document.app);
});
